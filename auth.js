const jwt = require("jsonwebtoken");

const secret = "CourseBookingAPI";

// Token Creation
module.exports.createAccessToken = (user) => {

	/* {
  _id: new ObjectId("63dc65b0ac5ee51c7bf67751"),
  firstName: 'John',
  lastName: 'Smith',
  email: 'john@mail.com',
  password: '$2b$10$uYall4/lSKkSfZBf0/aMbeA6Dku5O4RtxKPGIOuozuYQ96rjqV8oO',
  isAdmin: false,
  mobileNo: '09123456789',
  enrollments: [],
  _v: 0
  */

	//payload
	const data = {
		id: user._id,
		email: user.email,
		isAdmin: user.isAdmin
	}

	//jwt.sign(data/payload, secretkey, options);
	return jwt.sign(data, secret, {});

}


//Token Verification

module.exports.verify = (req, res, next) => {

	let token = req.headers.authorization;

	if(typeof token !== "undefined") {

		console.log(token);

		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {
			if(error){
				return res.send({auth: "failed"});
			} else {
				next();
			}
		})
	} else {
		return res.send({auth: "failed"});
	}

}


//Token Decryption
module.exports.decode = (token) => {

	if(typeof token !== "undefined") {
		token = token.slice(7, token.length);

		return jwt.verify(token, secret, (error, data) => {

			if (error){
				return null
			} else {
				return jwt.decode(token, {complete: true}).payload;
			}
		})

	} else {
		return null
	}
}